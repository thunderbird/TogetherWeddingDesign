<?xml version="1.0" encoding="UTF-8"?>
<configuration scan="true" scanPeriod="60 seconds" debug="false">
	<property name="LOG_ROOT" value="../logs" />
	<property name="MAX_DAILY_LENGTH" value="30" />
	<property name="MIN_ROLLING_INDEX" value="1" />
	<property name="MAX_ROLLING_SIZE" value="10" />
	<property name="LOG_PATTERN" value="%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{26} | %class:%line - %msg%n" />
	<property name="DAILY_TIME_PATTERN" value="%d{yyyy-MM-dd}" />

	<!-- 标准输出 -->
	<appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	<!-- 控制台输出 -->
	<appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>DEBUG</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	<!-- 每天文件输出Debug -->
	<appender name="LOG.DAILY.FILE.DEBUG" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>DEBUG</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
		<file>${LOG_ROOT}/daily-debug.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<fileNamePattern>${LOG_ROOT}/daily-debug.log.${DAILY_TIME_PATTERN}</fileNamePattern>
			<maxHistory>${MAX_DAILY_LENGTH}</maxHistory>
		</rollingPolicy>
		<append>true</append>
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	<!-- 每天文件输出Info -->
	<appender name="LOG.DAILY.FILE.INFO" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>INFO</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
		<file>${LOG_ROOT}/daily-info.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<fileNamePattern>${LOG_ROOT}/daily-info.log.${DAILY_TIME_PATTERN}</fileNamePattern>
			<maxHistory>${MAX_DAILY_LENGTH}</maxHistory>
		</rollingPolicy>
		<append>true</append>
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	<!-- 每天文件输出Error -->
	<appender name="LOG.DAILY.FILE.ERROR" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>ERROR</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
		<file>${LOG_ROOT}/daily-error.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<fileNamePattern>${LOG_ROOT}/daily-error.log.${DAILY_TIME_PATTERN}</fileNamePattern>
			<maxHistory>${MAX_DAILY_LENGTH}</maxHistory>
		</rollingPolicy>
		<append>true</append>
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	<!-- 每天文件输出Warn -->
	<appender name="LOG.DAILY.FILE.WARN" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>WARN</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
		<file>${LOG_ROOT}/daily-warn.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<fileNamePattern>${LOG_ROOT}/daily-warn.log.${DAILY_TIME_PATTERN}</fileNamePattern>
			<maxHistory>${MAX_DAILY_LENGTH}</maxHistory>
		</rollingPolicy>
		<append>true</append>
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	
	<!-- 滚动文件输出Debug -->
	<appender name="LOG.FIXED.FILE.DEBUG" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>DEBUG</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
		<file>${LOG_ROOT}/fixed-debug.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
			<fileNamePattern>fixed-debug.%i.log</fileNamePattern>
			<minIndex>${MIN_ROLLING_INDEX}</minIndex>
			<maxIndex>${MAX_ROLLING_SIZE}</maxIndex>
		</rollingPolicy>
		<append>true</append>
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	<!-- 滚动文件输出Info -->
	<appender name="LOG.FIXED.FILE.INFO" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>INFO</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
		<file>${LOG_ROOT}/fixed-info.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
			<fileNamePattern>fixed-info.%i.log</fileNamePattern>
			<minIndex>${MIN_ROLLING_INDEX}</minIndex>
			<maxIndex>${MAX_ROLLING_SIZE}</maxIndex>
		</rollingPolicy>
		<append>true</append>
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	<!-- 滚动文件输出Error -->
	<appender name="LOG.FIXED.FILE.ERROR" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>ERROR</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
		<file>${LOG_ROOT}/fixed-error.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
			<fileNamePattern>fixed-error.%i.log</fileNamePattern>
			<minIndex>${MIN_ROLLING_INDEX}</minIndex>
			<maxIndex>${MAX_ROLLING_SIZE}</maxIndex>
		</rollingPolicy>
		<append>true</append>
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	<!-- 滚动文件输出Warn -->
	<appender name="LOG.FIXED.FILE.WARN" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>WARN</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
		<file>${LOG_ROOT}/fixed-warn.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
			<fileNamePattern>fixed-warn.%i.log</fileNamePattern>
			<minIndex>${MIN_ROLLING_INDEX}</minIndex>
			<maxIndex>${MAX_ROLLING_SIZE}</maxIndex>
		</rollingPolicy>
		<append>true</append>
		<encoder>
			<pattern>
				${LOG_PATTERN}
			</pattern>
		</encoder>
	</appender>
	
	<logger name="com.twd.web" level="DEBUG" additivity="false">
		<appender-ref ref="CONSOLE" />
		<appender-ref ref="LOG.FIXED.FILE.DEBUG	" />
		<appender-ref ref="LOG.FIXED.FILE.INFO" />
		<appender-ref ref="LOG.FIXED.FILE.ERROR" />
	</logger>
	
	<root level="DEBUG">
		<appender-ref ref="STDOUT" />
	</root>
</configuration>
//
// Built on Wed Jul 30 11:15:28 CEST 2014 by logback-translator
// For more information on configuration files in Groovy
// please see http://logback.qos.ch/manual/groovy.html

// For assistance related to this tool or configuration files
// in general, please contact the logback user mailing list at
//    http://qos.ch/mailman/listinfo/logback-user

// For professional support please see
//   http://www.qos.ch/shop/products/professionalSupport

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.filter.LevelFilter
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy

import static ch.qos.logback.classic.Level.DEBUG
import static ch.qos.logback.classic.Level.ERROR
import static ch.qos.logback.classic.Level.INFO
import static ch.qos.logback.classic.Level.WARN
import static ch.qos.logback.core.spi.FilterReply.ACCEPT
import static ch.qos.logback.core.spi.FilterReply.DENY

scan("60 seconds")
def LOG_ROOT = "../logs"
def MAX_DAILY_LENGTH = 30
def MIN_ROLLING_INDEX = 1
def MAX_ROLLING_SIZE = 10
def LOG_PATTERN = "%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{26} | %class:%line - %msg%n"
def DAILY_TIME_PATTERN = "%d{yyyy-MM-dd}"
def MAX_FILE_SIZE = "5MB"

// 获取web项目中WEB-INFclasses的绝对路径
String strPath = this.getClass().getResource("/").getPath();
// 将开头的斜杠去掉
strPath = strPath.substring(1, strPath.length());
// 如果获取的路径中有空格，
strPath = strPath.replace("%20", " ");

//Console 输出
appender("STDOUT", ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}
appender("CONSOLE", ConsoleAppender) {
  filter(LevelFilter) {
    level = DEBUG
    onMatch = ACCEPT
    onMismatch = DENY
  }
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}

//每天输出
appender("LOG.DAILY.FILE.DEBUG", RollingFileAppender) {
  filter(LevelFilter) {
    level = DEBUG
    onMatch = ACCEPT
    onMismatch = DENY
  }
  file = strPath + "${LOG_ROOT}/daily-debug.log"
  rollingPolicy(TimeBasedRollingPolicy) {
    fileNamePattern = strPath + "${LOG_ROOT}/daily-debug.log.${DAILY_TIME_PATTERN}"
    maxHistory = MAX_DAILY_LENGTH
  }
  append = true
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}
appender("LOG.DAILY.FILE.INFO", RollingFileAppender) {
  filter(LevelFilter) {
    level = INFO
    onMatch = ACCEPT
    onMismatch = DENY
  }
  file = strPath + "${LOG_ROOT}/daily-info.log"
  rollingPolicy(TimeBasedRollingPolicy) {
    fileNamePattern = strPath + "${LOG_ROOT}/daily-info.log.${DAILY_TIME_PATTERN}"
    maxHistory = MAX_DAILY_LENGTH
  }
  append = true
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}
appender("LOG.DAILY.FILE.ERROR", RollingFileAppender) {
  filter(LevelFilter) {
    level = ERROR
    onMatch = ACCEPT
    onMismatch = DENY
  }
  file = strPath + "${LOG_ROOT}/daily-error.log"
  rollingPolicy(TimeBasedRollingPolicy) {
    fileNamePattern = "${LOG_ROOT}/daily-error.log.${DAILY_TIME_PATTERN}"
    maxHistory = MAX_DAILY_LENGTH
  }
  append = true
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}
appender("LOG.DAILY.FILE.WARN", RollingFileAppender) {
  filter(LevelFilter) {
    level = WARN
    onMatch = ACCEPT
    onMismatch = DENY
  }
  file = strPath + "${LOG_ROOT}/daily-warn.log"
  rollingPolicy(TimeBasedRollingPolicy) {
    fileNamePattern = "${LOG_ROOT}/daily-warn.log.${DAILY_TIME_PATTERN}"
    maxHistory = MAX_DAILY_LENGTH
  }
  append = true
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}

//固定文件输出
appender("LOG.FIXED.FILE.DEBUG", RollingFileAppender) {
  filter(LevelFilter) {
    level = DEBUG
    onMatch = ACCEPT
    onMismatch = DENY
  }
  file = strPath + "${LOG_ROOT}/fixed-debug.log"
  rollingPolicy(FixedWindowRollingPolicy) {
    fileNamePattern = strPath + "${LOG_ROOT}/fixed-debug.%i.log"
    minIndex = MIN_ROLLING_INDEX
    maxIndex = MAX_ROLLING_SIZE
  }
  triggeringPolicy(SizeBasedTriggeringPolicy) {
    maxFileSize = "${MAX_FILE_SIZE}"
  }
  append = true
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}
appender("LOG.FIXED.FILE.INFO", RollingFileAppender) {
  filter(LevelFilter) {
    level = INFO
    onMatch = ACCEPT
    onMismatch = DENY
  }
  file = strPath + "${LOG_ROOT}/fixed-info.log"
  rollingPolicy(FixedWindowRollingPolicy) {
    fileNamePattern = strPath + "${LOG_ROOT}/fixed-info.%i.log"
    minIndex = MIN_ROLLING_INDEX
    maxIndex = MAX_ROLLING_SIZE
  }
  triggeringPolicy(SizeBasedTriggeringPolicy) {
    maxFileSize = "${MAX_FILE_SIZE}"
  }
  append = true
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}
appender("LOG.FIXED.FILE.ERROR", RollingFileAppender) {
  filter(LevelFilter) {
    level = ERROR
    onMatch = ACCEPT
    onMismatch = DENY
  }
  file = strPath + "${LOG_ROOT}/fixed-error.log"
  rollingPolicy(FixedWindowRollingPolicy) {
    fileNamePattern = strPath + "${LOG_ROOT}/fixed-error.%i.log"
    minIndex = MIN_ROLLING_INDEX
    maxIndex = MAX_ROLLING_SIZE
  }
  triggeringPolicy(SizeBasedTriggeringPolicy) {
    maxFileSize = "${MAX_FILE_SIZE}"
  }
  append = true
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}
appender("LOG.FIXED.FILE.WARN", RollingFileAppender) {
  filter(LevelFilter) {
    level = WARN
    onMatch = ACCEPT
    onMismatch = DENY
  }
  file = strPath + "${LOG_ROOT}/fixed-warn.log"
  rollingPolicy(FixedWindowRollingPolicy) {
    fileNamePattern = strPath + "${LOG_ROOT}/fixed-warn.%i.log"
    minIndex = MIN_ROLLING_INDEX
    maxIndex = MAX_ROLLING_SIZE
  }
  triggeringPolicy(SizeBasedTriggeringPolicy) {
    maxFileSize = "${MAX_FILE_SIZE}"
  }
  append = true
  encoder(PatternLayoutEncoder) {
    pattern = "${LOG_PATTERN}"
  }
}
//配置Logger输出
logger("com.twd.web", DEBUG, ["CONSOLE", "LOG.FIXED.FILE.DEBUG ", "LOG.FIXED.FILE.INFO", "LOG.FIXED.FILE.ERROR","LOG.FIXED.FILE.WARN"], false)
//druid输出
logger("druid.sql", DEBUG, ["CONSOLE", "LOG.FIXED.FILE.DEBUG ", "LOG.FIXED.FILE.INFO", "LOG.FIXED.FILE.ERROR","LOG.FIXED.FILE.WARN"], false)
root(DEBUG, ["STDOUT"])
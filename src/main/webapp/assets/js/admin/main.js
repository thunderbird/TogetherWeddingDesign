/**
 * 后台Admin控制脚本
 */
function openAjaxTab(id, title, url) {
	var tabId = "tab_" + id;
	if (!$("#centerTab").tabs("exists", title)) {
		var $tab = $("<div></div>").attr("id", "tab_"+id);
		var tabString = $tab.html();
		$('#centerTab').tabs('add', {
			//在centerTab内新建一个tab,在里边加一个Iframe
			id:tabId,
			title: title,
			closable: true,
			cache: true,
			border: false,
			href: url,
			//注：使用iframe即可防止同一个页面出现js和css冲突的问题
			content: tabString
		});
		$.parser.parse($("#tab_" + tabId).parent());
	} else {
		//已创建就选中
		$('#centerTab').tabs('select', title);
	}
}

function formSubmit(id) {
	$("#" + id).submit();
}
/**
 * Front Base Javascript
 */

function fixWidth(id, minWidth, minHeight) {
	var screenWidth = window.innerWidth;
	if (minWidth == undefined || minWidth == null) {
		minWidth = 0;
	}
	if (minHeight == undefined || minHeight == null) {
		minHeight = 0;
	}
	$(id).css({
		width : screenWidth + "px",
	});
	// 重构页面渲染
	$(window).resize(function() {
		var screenHeight = window.innerHeight;
		var screenWidth = window.innerWidth;
		if (screenWidth > minWidth && screenHeight > minHeight) {
			$(id).css({
				width : screenWidth + "px",
			});
		}
	});
}

function fixHeight(id, minWidth, minHeight) {
	var screenHeight = window.innerHeight;
	if (minWidth == undefined || minWidth == null) {
		minWidth = 0;
	}
	if (minHeight == undefined || minHeight == null) {
		minHeight = 0;
	}
	$(id).css({
		height : screenHeight + "px"
	});
	// 重构页面渲染
	$(window).resize(function() {
		var screenHeight = window.innerHeight;
		var screenWidth = window.innerWidth;
		if (screenWidth > minWidth && screenHeight > minHeight) {
			$(id).css({
				height : screenHeight + "px"
			});
		}
	});
}
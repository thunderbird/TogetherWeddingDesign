<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	 <head>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/> 
		<title>Admin System</title>
		<!-- Base CSS -->
		<link rel="stylesheet" type="text/css" href="assets/css/base.css" />  
		<!-- jQuery -->
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
		 <!--Uploadify--> 
		<script type="text/javascript" src="assets/uploadify/jquery.uploadify.min.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/uploadify/uploadify.css" />
		<!-- EasyUI -->
		<script type="text/javascript" src="assets/easyui/jquery.easyui.min.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/easyui/themes/metro/easyui.css" />  
		<link rel="stylesheet" type="text/css" href="assets/easyui/themes/icon.css" />
		<!-- admin js -->
		<script type="text/javascript" src="assets/js/admin/main.js"></script>
		<!-- admin css -->
		<link rel="stylesheet" type="text/css" href="assets/css/admin/main.css" />  
	</head>
	<!-- 设置了class就可在进入页面加载layout -->
	<body class="easyui-layout">
		<!-- 正上方panel -->
		<div region="north" style="height:50px;padding:10px;">
		</div>
		<!-- 正左方panel -->
		<div region="west" title="Menu" split="true"
			 style="width:210px;padding1:1px;overflow:hidden;">
			<div class="easyui-accordion" fit="true" border="false" >
				<ul class="menuList">
					<li><a href="javascript:void(0)" onclick="openAjaxTab('UserConfig','UserConfig','__ROOT__/Admin/User/Config');">User Manager</a></li>
				</ul>
			</div>
		</div>
		<!-- 正中间panel -->
		<div region="center" title="Worksplaces">
			<div class="easyui-tabs" id="centerTab" fit="true" border="false">
				<div title="Welcome" style="padding:20px;overflow:hidden;" data-options="closable:true">
					<div style="margin-top:20px;">
						<span style="font-size: 14pt;">Welcome to Admin System!</span>
					</div>
				</div>
			</div>
		</div>
		<!-- 正下方panel -->
		<div region="south" style="height:30px;" align="center">
		</div>
	</body>
</html>

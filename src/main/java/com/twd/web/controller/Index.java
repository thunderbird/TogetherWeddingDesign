package com.twd.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("com.twd.web.controller.Index")
public class Index {
	
	private static final Logger logger = LoggerFactory.getLogger(Index.class);
	
	@RequestMapping("/index")
	public ModelAndView index(){
		ModelAndView mav = new ModelAndView("index"); 
		mav.addObject("test", "ChecKTest");
        return mav;  
	}
}
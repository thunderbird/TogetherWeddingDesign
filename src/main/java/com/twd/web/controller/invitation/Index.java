package com.twd.web.controller.invitation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("com.twd.web.controller.invitation.Index")
@RequestMapping("/invitation")
public class Index {
	
	private static final Logger logger = LoggerFactory.getLogger(Index.class);
	
	@RequestMapping("/index")
	public ModelAndView index(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav = new ModelAndView(); 
		mav.addObject("test", "checkTest");
        return mav;  
	}
	
	@RequestMapping("/test")
	public void test(Model m){
		m.addAttribute("test", "checkTest");
	}
}
	